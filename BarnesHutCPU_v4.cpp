//============================================================================
// Name        : BarnesHutCPU.cpp
// Author      : Dominik Thomas
// Version     :
// Copyright   : Copyright 2016 Dominik Thomas
// Description : Hello World in C, Ansi-style
//============================================================================

#include <boost/program_options.hpp>


#include <iostream>
#include <cmath>
#include <random>
#include <map>
#include <fstream>
#include <string>
#include <mutex>
#include <chrono>

// TODO add verbose mode

//#define INFO_MODE // Used for infos
#define USE_CPU
#define USE_PARALLEL
#define USE_MINMAX_ELEMENT

#ifdef USE_CPU
#include <vector>
#include <unordered_map>
#include <boost/iterator/counting_iterator.hpp>


// system namespace for datatypes and algorithms (std::pair or thrust::pair etc.)
namespace sys = std;

template <typename T>
using counting_iterator = boost::counting_iterator<T>;

#ifdef USE_PARALLEL
#include <parallel/algorithm>

namespace policy = __gnu_parallel;
#else
#include <algorithm>
#include <queue>

namespace policy = std;
#endif

#endif // TODO else USE GPU

#ifdef USE_GPU
#include <thrust/for_each.h>

namespace sys = thrust;
#endif


//#ifdef USE_PAR_EXECUTION_POLICY
//#define EXECUTION_POLICY std::execution::par
//#else
//#define EXECUTION_POLICY std::execution::seq
//#endif

namespace po = boost::program_options;



namespace BH {



// TODO ifdef USE_GPU

// Datatypes
/*
//                    min_x, max_x, min_y, max_y, min_z, max_z
typedef thrust::tuple<double, double, double, double, double, double> bbox_type; // boundarybox

typedef std::vector<bbox_type> bboxvec_type;
// position vector
//                               x                               y                             z
typedef thrust::tuple<std::vector<double>, std::vector<double>, std::vector<double> > posvec_type;
// velocity vector
//                             v_x                             v_y                           v_z
typedef thrust::tuple<std::vector<double>, std::vector<double>, std::vector<double> > velvec_type;
 */


// TODO alle std::cout auf printfs umstellen

// Build tree


const int N=200;//1200; // TODO remove comments
const int timesteps=5000; // 500
bool use_timer;
// TODO change
//const double G = 6.67408e-11;
const double G = 50000; //1;

// TODO replace .at functions by operator[], because then no bounds checkings are active (faster)
// TODO code analyzer
// TODO make viewer
// TODO replace std::for_each and all std:: with sequentiel version of current system

struct Particle {
	double m;
	double x, y, z;
	double v_x, v_y, v_z;
	double a_x, a_y, a_z;
};

struct TreeNode {
	double m;
	double m_x, m_y, m_z;
	// max dividable morton code wich is pow of 8^î
	uint64_t max_morton8;
	double boxsize;
	int level;
	// TODO remove min max, if not needed
	double min_x, min_y, min_z;
	double max_x, max_y, max_z;
	int parentnode; // TODO if not uses
	sys::pair<int, int> subnodes, particles;
};

typedef sys::vector<TreeNode> TreeNodeVector;
typedef TreeNodeVector::iterator TreeNodeVectorIterator;

sys::vector<Particle> particles(N);
sys::vector<sys::vector<TreeNode>> tree;

// Minimum and maximum coordinates
// TODO remove because they are now implicit in the root node
double min_x, min_y, min_z;
double max_x, max_y, max_z;
static double d; // max. needed axis length

void generateRandomEntrys() // TODO replace with thrust tabulate
{
	std::srand(0);
	std::default_random_engine gen(6);
	std::uniform_real_distribution<> dis1(-500000, 500000);
	std::uniform_real_distribution<> dis2(-1, 1);
	std::uniform_real_distribution<> dis3(1000000, 5000000);
	std::uniform_real_distribution<> dis4(-M_PI, M_PI);
	std::uniform_real_distribution<> dis5(-100, 100);
	auto random_pos = [&](){return dis1(gen);};
	auto random_vel = [&](){return dis2(gen);};
	//auto return_zero = [](){return 0;};
	// auto rdis2 = [&](){return dis2(gen);};
	//auto rdis3 = [](){return 3;};//[&](){return dis3(gen);};

	auto rdis6 = [&](){return dis3(gen);};//[&](){return dis3(gen);};

	policy::generate(particles.begin(), particles.end(), [&]{
		//		Particle p = {rdis6(), random_pos(), random_pos(), random_pos(),
		//				random_vel, random_vel, random_vel,
		//				0, 0, 0};
		Particle p;
		p.m = rdis6();
		p.x = random_pos();
		p.y = random_pos();
		p.z = random_pos();
		p.v_x = 0; //random_vel();
		p.v_y = 0; //random_vel();
		p.v_z = 0; //random_vel();
		p.a_x = p.a_y = p.a_z = 0;
		return p;
	});
}



// method to seperate bits from a given integer 3 positions apart
inline uint64_t splitBy3(unsigned int a) {
	uint64_t x = a & 0x1fffff; // we only look at the first 21 bits
	x = (x | x << 32) & 0x1f00000000ffff;  // shift left 32 bits, OR with self, and 00011111000000000000000000000000000000001111111111111111
	x = (x | x << 16) & 0x1f0000ff0000ff;  // shift left 32 bits, OR with self, and 00011111000000000000000011111111000000000000000011111111
	x = (x | x << 8) & 0x100f00f00f00f00f; // shift left 32 bits, OR with self, and 0001000000001111000000001111000000001111000000001111000000000000
	x = (x | x << 4) & 0x10c30c30c30c30c3; // shift left 32 bits, OR with self, and 0001000011000011000011000011000011000011000011000011000100000000
	x = (x | x << 2) & 0x1249249249249249;
	return x;
}


uint64_t convertPositionToMortonCode(const BH::Particle &p) {
	uint64_t answer = 0;
	unsigned int x = ((int64_t) p.x) + (1 << 20);
	unsigned int y = ((int64_t) p.y) + (1 << 20);
	unsigned int z = ((int64_t) p.z) + (1 << 20);

	answer |= splitBy3(x) | splitBy3(y) << 1 | splitBy3(z) << 2;
	// return answer;
	return x;
}

// TODO for auto Schleife benutzen
void printParticleData() {
	for(int i = 0; i < N; i++) 	{
		std::cout << i << ". Morton: (" << convertPositionToMortonCode(particles[i]) << ") Mass:" << particles[i].m;
		std::cout << " {" << particles[i].x << "," << particles[i].y << "," << particles[i].z << "}";
		std::cout << " v{" << particles[i].v_x << "," << particles[i].v_y << "," << particles[i].v_z << "}";
		std::cout << " a{" << particles[i].a_x << "," << particles[i].a_y << "," << particles[i].a_z << "}" << std::endl;
	}
}


void printTree() {
	std::cout << "Printing tree vector: " << std::endl;
	int i = 0;
	// strict sequentially
	std::for_each(tree.begin(), tree.end(), [&](const TreeNodeVector &tnv) {
		std::cout << i << ": ";
		int j = 0;
		std::for_each(tnv.begin(), tnv.end(), [&](const TreeNode &tn) {
			std::cout << j << ": ";
			std::cout << tn.m << "{" << tn.m_x << ", " << tn.m_y << ", " << tn.m_z << "} ";
			std::cout << "S(" << tn.subnodes.first  << ", " << tn.subnodes.second  << "), ";
			std::cout << "P(" << tn.particles.first << ", " << tn.particles.second << "); ";
			++j;
		});
		std::cout << std::endl;
		++i;
	});
}


uint64_t getUpperPowerOf8(uint64_t i) {
	// get log_8(i)
	int pow = std::log(i)/std::log(8);
	//ceil
	pow = std::ceil(pow);
	return std::pow(8, pow);
}

// TODO merge two particles with same morton code

// Morton encode with magic bits by Jeroen Baert
void sortParticlesInMortonOrder() {
	policy::sort(particles.begin(), particles.end(), [&](const Particle &a, const Particle &b) {
		uint64_t morton_a = convertPositionToMortonCode(a);
		uint64_t morton_b = convertPositionToMortonCode(b);
		return morton_a < morton_b;
	});
}

// returns true, if contains no particles
bool isEmpty(const TreeNode &treenode) {
	if (treenode.particles.second == 0) {
		return true;
	}
	return false;
}

// returns true, if node has no subnodes
bool isLeaf(const TreeNode &treenode) {
	int last_particle = treenode.particles.second-1;
	int first_particle = treenode.particles.first;
	if (last_particle > 0 && last_particle - first_particle > 0) {
		return false;
	}
	return true;
}

bool allLeaves(TreeNodeVectorIterator begin, TreeNodeVectorIterator end) {
	TreeNodeVectorIterator notLeave = policy::find_if(begin, end, [](const TreeNode &tn) {
		return !isLeaf(tn);
	});
	if (notLeave == end)
		return true;
	return false;
}

bool hasSubnodes(const TreeNode &treenode) {
	// has only subnodes if first entry and second differ
	if (treenode.subnodes.first == treenode.subnodes.second) {
		return false;
	}
	return true;
}

bool hasOneParticle(const TreeNode &treenode) {
	// has only subnodes if first entry and second differ
	if (treenode.particles.first == treenode.particles.second-1) {
		return true;
	}
	return false;
}

void buildTree() {
	// TODO delete old tree

	sortParticlesInMortonOrder();


	// TODO remove if not necessary
	////////////////////////////////////////////////
	auto cmp_x = [](const Particle &a, const Particle &b) {
		return a.x < b.x;
	};
	auto cmp_y = [](const Particle &a, const Particle &b) {
		return a.y < b.y;
	};
	auto cmp_z = [](const Particle &a, const Particle &b) {
		return a.z < b.z;
	};

	decltype(particles)::iterator minEl_x, maxEl_x;
	decltype(particles)::iterator minEl_y, maxEl_y;
	decltype(particles)::iterator minEl_z, maxEl_z;


	sys::tie(minEl_x, maxEl_x) = sys::minmax_element(particles.begin(), particles.end(), cmp_x);
	sys::tie(minEl_y, maxEl_y) = sys::minmax_element(particles.begin(), particles.end(), cmp_y);
	sys::tie(minEl_z, maxEl_z) = sys::minmax_element(particles.begin(), particles.end(), cmp_z);

	min_x = minEl_x->x;
	max_x = maxEl_x->x;
	min_y = minEl_y->y;
	max_y = maxEl_y->y;
	min_z = minEl_x->z;
	max_z = maxEl_x->z;

	double d_x = max_x - min_x;
	double d_y = max_y - min_y;
	double d_z = max_z - min_z;

	// using e.g. thrust::minimum would be a little overpowered here.
	double d_tmp = (d_x > d_y) ? d_x : d_y;
	d = (d_tmp > d_z) ? d_tmp : d_z;

	std::cout << "d: " << d << " Min: (" << min_x << ", " << min_y << ", " << min_z << ")\nMax: (" << max_x << ", " << max_y << ", " << max_z << ")" << std::endl;
	////////////////////////////////////////////////

	// add root Treenode
	tree.clear();
	TreeNode root = {}; // value initialize all members
	// all are initialized to 0 by compiler
	// Add first particle
	root.particles = sys::make_pair(0,N);
	uint64_t max_morton = convertPositionToMortonCode(particles[N-1]);
	root.max_morton8 = getUpperPowerOf8(max_morton);
	/*
	root.min_x = min_x;
	root.min_y = min_y;
	root.min_z = min_z;
	root.max_x = min_x + d;
	root.max_y = min_y + d;
	root.max_z = min_z + d;
	 */
	root.level = 0;
	tree.push_back(sys::vector<TreeNode>(1, root));



	std::mutex m_insert, m_move;

	// explicit sequentiality
	for (int l = 0; !allLeaves(tree[l].begin(), tree[l].end()); ++l) {
		// Add new tree level
		tree.push_back(sys::vector<TreeNode>());
		for(auto tn_it = tree[l].begin(); tn_it != tree[l].end(); ++tn_it) {
			// TODO if leave do nothing, just add to sublevel
			if (isLeaf(*tn_it)) {
				tree[l+1].push_back(*tn_it);
				continue;
			}

			// no leave, so subdivide
			// max. morton code which is dividable by 8
			Particle max_particle = particles[tn_it->particles.second-1];
			int morton_max_particle = convertPositionToMortonCode(max_particle);
			int remainder = morton_max_particle % 8;
			int max_morton = (remainder == 0) ? morton_max_particle : morton_max_particle + 8 - remainder;
			// now divide in 8 parts again and again
			int step = max_morton/8;

			// Now find all values lower max_morton/8, max_morton/8+step... and put in own treenode

			sys::vector<TreeNode> octants(8, TreeNode());

			// std::cout << "Maxmorton" << max_morton << std::endl;


			// O(1) (O(8))
			int sub_mortoncode = step;
			for(TreeNodeVectorIterator it = octants.begin(); it != octants.end(); ++it) {
				it->max_morton8 = sub_mortoncode;
				//std::cout << "octantmorton " << it->max_morton8 << std::endl;
				sub_mortoncode += step;
			};

			// Now find the 8 positions where to subseparate tn
			TreeNodeVectorIterator it = octants.begin();
			it->particles.first = tn_it->particles.first;


			// max. O(N) usually O(log(N))
			for (int i = tn_it->particles.first; i < tn_it->particles.second; ++i) {
				if (convertPositionToMortonCode(particles[i]) > it->max_morton8) { // now split
					// std::cout << "Part.Morton: " << convertPositionToMortonCode(particles[i]);
					// std::cout << " Max-morton: " << it->max_morton8 << std::endl;
					it->particles.second = i;

					// std::cout << "i:" << i << std::endl;

					if(it->particles.first == it->particles.second) {
						// remove node, because it is empty
						it = octants.erase(it); // deletes element and iterator now points to following octant
					} else {
						++it; // only point to new octant
					}

					// New octants starts with this particle
					it->particles.first = i;
				}
			}
			// Close last octant
			it->particles.second = tn_it->particles.second;
			if(it->particles.first == it->particles.second) {
				// remove node, because it is empty
				octants.pop_back(); // delete last octant
			}

			for (auto it = octants.begin(); it != octants.end(); ++it) {
				if (it->particles.first == 0 && it->particles.second == 0) {
					it = octants.erase(it);
					--it;
				}
			}

			// Add nodes, at least one has to exist
			// get and save data where the subnodes are in the sublevel
			auto treesublevel_begin = tree[l+1].begin();
			auto treesublevel_old_end = tree[l+1].end();
			int subnodes_begin = sys::distance(treesublevel_begin, treesublevel_old_end);
			tree[l+1].insert(treesublevel_old_end, octants.begin(), octants.end());
			// old begin iterator gets sometimes invalid, if vector.capacity was lower than new size;
			int subnodes_end = sys::distance(tree[l+1].begin(), tree[l+1].end());
			tn_it->subnodes.first = subnodes_begin;
			tn_it->subnodes.second = subnodes_end;
		}
	}

}

void calcNodeMasses() {
	for (int l = tree.size()-1; l >= 0; --l) {
		policy::for_each(tree[l].begin(), tree[l].end(), [l](TreeNode &tn) {
			if (!hasSubnodes(tn)) { // hasSubnodes and isLeaf can be used both to check if it is leaf, but use different ways to get that
				Particle &p = particles[tn.particles.first];
				tn.m = p.m;
				tn.m_x = p.x;
				tn.m_y = p.y;
				tn.m_z = p.z;
				return;
			} // else
			// calc mass from subnodes
			double m_tot;
			double m_x, m_y, m_z;
			m_tot = m_x = m_y = m_z = 0;
			// O(1) ( max. O(8) )
			for (int i = tn.subnodes.first; i < tn.subnodes.second; ++i) {
				TreeNode sub_tn = tree[l+1][i];
				m_tot += sub_tn.m;
				m_x += (sub_tn.m * sub_tn.m_x);
				m_y += (sub_tn.m * sub_tn.m_y);
				m_z += (sub_tn.m * sub_tn.m_z);
			}
			tn.m = m_tot;
			tn.m_x = (m_x / m_tot);
			tn.m_y = (m_y / m_tot);
			tn.m_z = (m_z / m_tot);
			// use method also to add level information
			tn.level = l;
			tn.boxsize = d / (sys::pow(8.0, l));
		});
	}
}

// sys::hypot not possible to use, because not supported on all plattforms
double hypot(double x, double y, double z)
{
	return sys::sqrt(x*x+y*y+z*z);
}

void calcAcc() {
	policy::for_each(particles.begin(), particles.end(), [](Particle& p) {
		// Now start from top to bottom
		double theta = 0.5; // TODO make theta changeable by user
		double acc_x, acc_y, acc_z;
		acc_x = acc_y = acc_z = 0;
		sys::vector<TreeNode> queue;
		queue.push_back(tree[0][0]);
		// old elements are not removed, because each remove costs O(N), so we use O(log) (worst case O(N) ) memory instead
		for (size_t i = 0; i < queue.size(); ++i) {
			const TreeNode& tn = queue[i];
			double l_x, l_y, l_z;
			l_x = tn.m_x - p.x;
			l_y = tn.m_y - p.y;
			l_z = tn.m_z - p.z;
			double l = hypot(l_x, l_y, l_z);
			// Skip if too near
			if (l<0.0001) { // TODO use minimal possible value
				continue;
			}
			// if particle or far away, use treenode data
			if (!hasSubnodes(tn) || tn.boxsize/l < theta) {
				//double factor = G*tn.m / sys::abs(sys::pow(l,2));
				double factor = 0.00000000000005;
				// TODO check if some is NaN or infty
				acc_x += factor*(l_x/l);
				acc_y += factor*(l_y/l);
				acc_z += factor*(l_z/l);
				continue;
				//return;
			} // else
			// add all subnodes
			for (int j = tn.subnodes.first; j < tn.subnodes.second; ++j) {
				queue.push_back(tree[tn.level+1][j]);
			}

		}
		p.a_x = acc_x;
		p.a_y = acc_y;
		p.a_z = acc_z;
	});
}

void updateData() {
	// TODO make timesteplength changeable
	double dt = 1*365*24*60*60; //one year in s

	// alternative to counting iterator is vector with elements from 0 to N
	// TODO use of tabulate maybe possible
	policy::for_each(particles.begin(), particles.end(), [&](Particle& p){
		p.v_x += p.a_x*dt;
		p.v_y += p.a_y*dt;
		p.v_z += p.a_z*dt;

		p.x += p.v_x*dt;
		p.y += p.v_y*dt;
		p.z += p.v_z*dt;
	});
}

void saveParticleData() {
	std::ofstream datafile;
	datafile.open ("data.xyz", std::ofstream::out | std::ofstream::app);
	datafile << N << std::endl;
	datafile << "#Comment" << std::endl;
	for (int i = 0; i < N; ++i) {
		const Particle& p = particles[i];
		datafile << "H " << p.x << " " << p.y << " " << p.z << std::endl;
	}
	datafile.close();
}


} // namespace BH



int main(int argc, char *argv[])
{
	std::cout << "CPU BarnesHut v0.01" << std::endl; //TODO use global version definition

	/*  __gnu_parallel::_Settings s;
	  s.algorithm_strategy = __gnu_parallel::force_parallel;
	  __gnu_parallel::_Settings::set(s); */

#ifdef USE_PARALLEL
	std::cout << "Using parallel mode" << std::endl;
#else
	std::cout << "Using sequential mode" << std::endl;
#endif

	if (true/*argc != 3*/) { // TODO change
		std::cerr << "arguments: number_of_bodies number_of_timesteps" << std::endl;
		//exit(-1);
	}




	std::cout << "Generate random entrys" << std::endl;
	BH::generateRandomEntrys();

	// remove particle data // TODO only when xyz is used
	std::system("rm data.xyz");
	std::cout << "Removed existin data.xyz" << std::endl;

	std::chrono::duration<double> treetime(0), massestime(0), acctime(0), updatetime(0), savetime(0);
	std::chrono::high_resolution_clock::time_point start, stop, start_tmp, stop_tmp;
	if(BH::use_timer)
		start = std::chrono::high_resolution_clock::now();
	for(int step = 0; step < BH::timesteps; ++step) {
		std::cout << "Step " << step << std::endl;
#ifdef INFO_MODE
		std::cout << "Build tree" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp = std::chrono::high_resolution_clock::now();
		BH::buildTree();
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			treetime += stop_tmp - start_tmp ;
		}
		//calcMass of branches

#ifdef INFO_MODE
		std::cout << "Calc node masses" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::calcNodeMasses();
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			massestime += stop_tmp - start_tmp ;
		}

#ifdef INFO_MODE
		std::cout << "Calc Acc" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::calcAcc(); // TODO make param with lambda expression
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			acctime += stop_tmp - start_tmp ;
		}

#ifdef INFO_MODE
		std::cout << "Update data" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::updateData(); // TODO nur speichern, wenn ein 250 MB groß, o.ä.
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			updatetime += stop_tmp - start_tmp ;
		}


		// BH::printTree();
		// BH::printParticleData();
#ifdef INFO_MODE
		std::cout << "Save data" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::saveParticleData();
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			savetime += stop_tmp - start_tmp ;
		}
	}


	if(BH::use_timer) {
		auto stop = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> duration =  stop - start;

		std::cout << "Finished in " << duration.count() << " s" << std::endl;
		std::cout << "Tree: " << treetime.count() << " Masses: " << massestime.count() << " Acc: " << acctime.count();
		std::cout << " Update: " << updatetime.count() << " Save: " << savetime.count() << std::endl;
	}


	return 0;
}
