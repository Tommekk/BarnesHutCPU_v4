CXX=g++-5

CXXFLAGS =	 -fopenmp -O0 -g -Wall -std=c++1z

OBJS =		BarnesHutCPU_v4.o

LIBS =

TARGET =	BarnesHutCPU_v4

$(TARGET):	$(OBJS)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(OBJS) $(LIBS) -lboost_program_options

all:	$(TARGET)

clean:
	rm -f $(OBJS) $(TARGET)
